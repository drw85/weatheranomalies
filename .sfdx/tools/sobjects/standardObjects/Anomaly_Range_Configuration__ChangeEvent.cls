// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Anomaly_Range_Configuration__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global SObject SetupOwner;
    global Id SetupOwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Double Max_Humidity__c;
    global Double Max_Pressure__c;
    global Double Max_Temperature__c;
    global Double Min_Humidity__c;
    global Double Min_Pressure__c;
    global Double Min_Temperature__c;

    global Anomaly_Range_Configuration__ChangeEvent () 
    {
    }
}