// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Weather__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global City__c City__c;
    global Double Cloudiness__c;
    global Double Humidity__c;
    global Double Maximum_Temperature__c;
    global Double Minimum_Temperature__c;
    global Double Pressure__c;
    global Double Temperature__c;
    global Double Temperature_Feels_Like__c;
    global Double Visibility__c;
    global Double Wind_Speed__c;

    global Weather__ChangeEvent () 
    {
    }
}