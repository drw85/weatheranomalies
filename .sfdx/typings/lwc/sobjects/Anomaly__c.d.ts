declare module "@salesforce/schema/Anomaly__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Anomaly__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Anomaly__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Anomaly__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Anomaly__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Anomaly__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Anomaly__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Anomaly__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Anomaly__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Anomaly__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Anomaly__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Anomaly__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Anomaly__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Anomaly__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Anomaly__c.City__r" {
  const City__r:any;
  export default City__r;
}
declare module "@salesforce/schema/Anomaly__c.City__c" {
  const City__c:any;
  export default City__c;
}
declare module "@salesforce/schema/Anomaly__c.City_Name__c" {
  const City_Name__c:string;
  export default City_Name__c;
}
declare module "@salesforce/schema/Anomaly__c.Reported__c" {
  const Reported__c:boolean;
  export default Reported__c;
}
declare module "@salesforce/schema/Anomaly__c.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Anomaly__c.Weather__r" {
  const Weather__r:any;
  export default Weather__r;
}
declare module "@salesforce/schema/Anomaly__c.Weather__c" {
  const Weather__c:any;
  export default Weather__c;
}
