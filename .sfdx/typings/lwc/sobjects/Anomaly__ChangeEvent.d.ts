declare module "@salesforce/schema/Anomaly__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.City__c" {
  const City__c:any;
  export default City__c;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.City_Name__c" {
  const City_Name__c:string;
  export default City_Name__c;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.Reported__c" {
  const Reported__c:boolean;
  export default Reported__c;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Anomaly__ChangeEvent.Weather__c" {
  const Weather__c:any;
  export default Weather__c;
}
