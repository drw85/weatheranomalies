declare module "@salesforce/schema/City__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/City__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/City__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/City__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/City__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/City__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/City__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/City__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/City__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/City__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/City__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/City__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/City__ChangeEvent.City_Code__c" {
  const City_Code__c:string;
  export default City_Code__c;
}
declare module "@salesforce/schema/City__ChangeEvent.Country__c" {
  const Country__c:string;
  export default Country__c;
}
declare module "@salesforce/schema/City__ChangeEvent.Location__Latitude__s" {
  const Location__Latitude__s:number;
  export default Location__Latitude__s;
}
declare module "@salesforce/schema/City__ChangeEvent.Location__Longitude__s" {
  const Location__Longitude__s:number;
  export default Location__Longitude__s;
}
declare module "@salesforce/schema/City__ChangeEvent.Location__c" {
  const Location__c:any;
  export default Location__c;
}
