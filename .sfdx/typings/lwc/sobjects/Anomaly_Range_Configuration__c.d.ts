declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.SetupOwner" {
  const SetupOwner:any;
  export default SetupOwner;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.SetupOwnerId" {
  const SetupOwnerId:any;
  export default SetupOwnerId;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Max_Humidity__c" {
  const Max_Humidity__c:number;
  export default Max_Humidity__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Max_Pressure__c" {
  const Max_Pressure__c:number;
  export default Max_Pressure__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Max_Temperature__c" {
  const Max_Temperature__c:number;
  export default Max_Temperature__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Min_Humidity__c" {
  const Min_Humidity__c:number;
  export default Min_Humidity__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Min_Pressure__c" {
  const Min_Pressure__c:number;
  export default Min_Pressure__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__c.Min_Temperature__c" {
  const Min_Temperature__c:number;
  export default Min_Temperature__c;
}
