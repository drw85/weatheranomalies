declare module "@salesforce/schema/Weather__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Weather__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Weather__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Weather__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Weather__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Weather__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Weather__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Weather__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Weather__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Weather__ChangeEvent.City__c" {
  const City__c:any;
  export default City__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Cloudiness__c" {
  const Cloudiness__c:number;
  export default Cloudiness__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Humidity__c" {
  const Humidity__c:number;
  export default Humidity__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Maximum_Temperature__c" {
  const Maximum_Temperature__c:number;
  export default Maximum_Temperature__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Minimum_Temperature__c" {
  const Minimum_Temperature__c:number;
  export default Minimum_Temperature__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Pressure__c" {
  const Pressure__c:number;
  export default Pressure__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Temperature__c" {
  const Temperature__c:number;
  export default Temperature__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Temperature_Feels_Like__c" {
  const Temperature_Feels_Like__c:number;
  export default Temperature_Feels_Like__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Visibility__c" {
  const Visibility__c:number;
  export default Visibility__c;
}
declare module "@salesforce/schema/Weather__ChangeEvent.Wind_Speed__c" {
  const Wind_Speed__c:number;
  export default Wind_Speed__c;
}
