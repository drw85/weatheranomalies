declare module "@salesforce/schema/Weather__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Weather__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Weather__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Weather__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Weather__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Weather__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Weather__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Weather__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Weather__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Weather__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Weather__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Weather__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Weather__c.City__r" {
  const City__r:any;
  export default City__r;
}
declare module "@salesforce/schema/Weather__c.City__c" {
  const City__c:any;
  export default City__c;
}
declare module "@salesforce/schema/Weather__c.Cloudiness__c" {
  const Cloudiness__c:number;
  export default Cloudiness__c;
}
declare module "@salesforce/schema/Weather__c.Humidity__c" {
  const Humidity__c:number;
  export default Humidity__c;
}
declare module "@salesforce/schema/Weather__c.Maximum_Temperature__c" {
  const Maximum_Temperature__c:number;
  export default Maximum_Temperature__c;
}
declare module "@salesforce/schema/Weather__c.Minimum_Temperature__c" {
  const Minimum_Temperature__c:number;
  export default Minimum_Temperature__c;
}
declare module "@salesforce/schema/Weather__c.Pressure__c" {
  const Pressure__c:number;
  export default Pressure__c;
}
declare module "@salesforce/schema/Weather__c.Temperature__c" {
  const Temperature__c:number;
  export default Temperature__c;
}
declare module "@salesforce/schema/Weather__c.Temperature_Feels_Like__c" {
  const Temperature_Feels_Like__c:number;
  export default Temperature_Feels_Like__c;
}
declare module "@salesforce/schema/Weather__c.Visibility__c" {
  const Visibility__c:number;
  export default Visibility__c;
}
declare module "@salesforce/schema/Weather__c.Wind_Speed__c" {
  const Wind_Speed__c:number;
  export default Wind_Speed__c;
}
