declare module "@salesforce/schema/Anomaly__Share.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Anomaly__Share.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/Anomaly__Share.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/Anomaly__Share.UserOrGroup" {
  const UserOrGroup:any;
  export default UserOrGroup;
}
declare module "@salesforce/schema/Anomaly__Share.UserOrGroupId" {
  const UserOrGroupId:any;
  export default UserOrGroupId;
}
declare module "@salesforce/schema/Anomaly__Share.AccessLevel" {
  const AccessLevel:string;
  export default AccessLevel;
}
declare module "@salesforce/schema/Anomaly__Share.RowCause" {
  const RowCause:string;
  export default RowCause;
}
declare module "@salesforce/schema/Anomaly__Share.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Anomaly__Share.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Anomaly__Share.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Anomaly__Share.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
