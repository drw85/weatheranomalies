declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.SetupOwner" {
  const SetupOwner:any;
  export default SetupOwner;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.SetupOwnerId" {
  const SetupOwnerId:any;
  export default SetupOwnerId;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Max_Humidity__c" {
  const Max_Humidity__c:number;
  export default Max_Humidity__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Max_Pressure__c" {
  const Max_Pressure__c:number;
  export default Max_Pressure__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Max_Temperature__c" {
  const Max_Temperature__c:number;
  export default Max_Temperature__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Min_Humidity__c" {
  const Min_Humidity__c:number;
  export default Min_Humidity__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Min_Pressure__c" {
  const Min_Pressure__c:number;
  export default Min_Pressure__c;
}
declare module "@salesforce/schema/Anomaly_Range_Configuration__ChangeEvent.Min_Temperature__c" {
  const Min_Temperature__c:number;
  export default Min_Temperature__c;
}
