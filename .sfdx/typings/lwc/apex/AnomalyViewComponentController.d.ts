declare module "@salesforce/apex/AnomalyViewComponentController.getCurrentWeather" {
  export default function getCurrentWeather(): Promise<any>;
}
declare module "@salesforce/apex/AnomalyViewComponentController.getAnomalies" {
  export default function getAnomalies(param: {searchingType: any, isOnlyUnreported: any}): Promise<any>;
}
declare module "@salesforce/apex/AnomalyViewComponentController.generateAnomaliesReport" {
  export default function generateAnomaliesReport(): Promise<any>;
}
