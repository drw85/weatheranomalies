({
    doInit: function (component, event, helper) {
        component.set("v.isLoading", true);
        let type = component.get('v.currentListViewName');
        let isOnlyUnreported = component.get('v.isOnlyUnreported');
        helper.getAnomaliesByType(component, event, helper, type, isOnlyUnreported);
    },
    unreportedOnCheck: function (component, event, helper) {
        component.set("v.isLoading", true);
        var checkbox = component.find("checkboxIsOnlyUnreported");
        component.set('v.isOnlyUnreported', checkbox.get('v.value'));
        let type = component.get('v.currentListViewName');
        let isOnlyUnreported = component.get('v.isOnlyUnreported');
        helper.getAnomaliesByType(component, event, helper, type, isOnlyUnreported);
    },
    getWeather: function (component, event, helper) {
        component.set("v.isLoading", true);
        var action = component.get("c.getCurrentWeather");
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                let type = component.get('v.currentListViewName');
                let isOnlyUnreported = component.get('v.isOnlyUnreported');
                helper.getAnomaliesByType(component, event, helper, type, isOnlyUnreported);
            } else {
                alert('Something went wrong');
            }
        });
        $A.enqueueAction(action);
    },
    handleListSelect: function (component, event, helper) {
        component.set("v.isLoading", true);
        let selectedMenuItemValue = event.getParam("value");
        let isOnlyUnreported = component.get('v.isOnlyUnreported');
        if (selectedMenuItemValue !== component.get("v.currentListViewName")) {
            component.set("v.currentListViewName", selectedMenuItemValue);
            helper.getAnomaliesByType(component, event, helper, selectedMenuItemValue, isOnlyUnreported);
        }
    },
    generateReport: function (component, event, helper) {
        component.set("v.isLoading", true);
        let action = component.get('c.generateAnomaliesReport');
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                let type = component.get('v.currentListViewName');
                let isOnlyUnreported = component.get('v.isOnlyUnreported');
                helper.getAnomaliesByType(component, event, helper, type, isOnlyUnreported);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Generating report",
                    "message": "Report generated successfully!",
                    "type": "success"
                });
                toastEvent.fire();
            } else {
                alert('Something went wrong');
            }
        })
        $A.enqueueAction(action);
    },
});