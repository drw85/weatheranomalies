({
    getAnomaliesByType: function (component, event, helper, searchingType, isOnlyUnreported) {
        let action = component.get('c.getAnomalies');
        action.setParam('searchingType', searchingType);
        action.setParam('isOnlyUnreported', isOnlyUnreported);
        action.setCallback(this, function (response) {
            let anomalies = response.getReturnValue();
            component.set('v.anomalyList', anomalies);
            component.set("v.isLoading", false);
            if(anomalies.length > 0){
                component.set('v.noAnomalyFound', false);
            } else {
                component.set('v.noAnomalyFound', true);
            }
        });
        $A.enqueueAction(action);
    }
})