import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


import getAdr from '@salesforce/apex/AnomalyReportEmailSenderController.getUserEmails';
import doSend from '@salesforce/apex/AnomalyReportEmailSenderController.sendEmailWithReport'

export default class AnomalyReportEmailSenderComponent extends LightningElement {
    @api recordId;
    selected = [];
    options;
    @track btnState = true;
    
    connectedCallback() {
        getAdr()
            .then(result => {
                this.options = [];
                result.forEach(user => {
                    this.options.push({ 'label': user.Name + '(' + user.Email + ')', 'value': user.Id });
                });
            });
    }

    handleChange(e) {
        this.selected = e.detail.value;
        if (this.selected.length > 0) {
            this.btnState = false;
        } else {
            this.btnState = true;
        }
    }

    handleSendReport() {
        doSend({ anomalyReportId: this.recordId, selectedUsers: this.selected })
            .then((result) => {
                    const evt = new ShowToastEvent({
                        title: 'Anomaly report sending',
                        message: 'Anomaly report sended successfully',
                        variant: 'success',
                    });
                    this.dispatchEvent(evt);
            })
            .catch((error) => {
                const evt = new ShowToastEvent({
                    title: 'Anomaly report sending',
                    message: JSON.stringify(error),
                    variant: 'error',
                });
                this.dispatchEvent(evt);
            });
    }
}