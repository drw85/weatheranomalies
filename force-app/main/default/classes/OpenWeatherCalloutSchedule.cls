global class OpenWeatherCalloutSchedule implements Schedulable {
    
    public static void startDailyAt8Am(){
        OpenWeatherCalloutSchedule job = new OpenWeatherCalloutSchedule();
        String cron = '0 8 * * *';
        System.schedule('DailyOpenWeatherCallout', cron, job);
    }
    
    public void execute(SchedulableContext ctx) {
        OpenWeatherCalloutService.getWeatherFromOpenWeather();
    }
}