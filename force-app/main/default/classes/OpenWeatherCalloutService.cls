public class OpenWeatherCalloutService {

    public static String API_URL = 'http://api.openweathermap.org/data/2.5/weather?q=';
    public static String API_KEY = '626a7d0a9d30bf15c1c017d78657b76e';
    public static List<String> CITIES = new String[]{'Tokyo', 'Osaka', 'Kyoto'};

    public static void getWeatherFromOpenWeather() {
        List<City__c> citiesFromDb = [SELECT iD, Name FROM City__c WHERE Name IN :CITIES];
        List<WeatherResponse> responses = new List<WeatherResponse>();
        List<Weather__c> weathers = new List<Weather__c>();
        for (WeatherResponse wr : getRawData(citiesFromDb)) {
            Weather__c w = generateWeather(wr);
            weathers.add(w);
        }
    insert weathers;
}

    public static List<WeatherResponse> getRawData(List<City__c> cities){
        List<WeatherResponse> data = new List<WeatherResponse>();
        for (City__c city : cities) {
            HttpResponse response = sendRequest(city.Name);
            if(response.getStatusCode() == 200){
                WeatherResponse wr = (WeatherResponse)JSON.deserialize(response.getBody(), WeatherResponse.class);
                wr.city = city;
                data.add(wr); 
            }
        }
        return data;
    }

    public static Weather__c generateWeather(WeatherResponse wr){
        Weather__c weather = new Weather__c(
        City__c = wr.city.iD,    
        Temperature__c = wr.main.temp,
            Temperature_Feels_Like__c = wr.main.feels_like,
            Minimum_Temperature__c = wr.main.temp_min,
            Maximum_Temperature__c = wr.main.temp_max,
            Pressure__c = wr.main.pressure,
            Humidity__c = wr.main.humidity
        );
        if(wr.clouds != null) {
            weather.Cloudiness__c = wr.clouds.all;
        }
        if(wr.wind != null) {
            weather.Wind_Speed__c = wr.wind.speed;
        }
        return weather;
    }

    public static HttpResponse sendRequest(String city){
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        String endpoint = API_URL + city + '&appid=' + API_KEY;
        req.setEndpoint(endpoint);
        HttpResponse resp = http.send(req);
        return resp;
    }
    
    public class WeatherResponse {
       public String name;
       public Main main;
       public Integer visibility;
       public Wind wind;
       public Clouds clouds;
       public City__c city;
    }

    public class Main{
        public Double temp;
        public Double feels_like;
        public Double temp_min;
        public Double temp_max;
        public Double pressure;
        public Integer humidity;
    }

    public class Wind{
        public Double speed;
    }

    public class Clouds{
        public Integer all;
    }
}