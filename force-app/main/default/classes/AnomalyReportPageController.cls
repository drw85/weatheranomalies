public class AnomalyReportPageController {
    
    private static String TEMP_ANOMALY = 'Temperature Anomaly';
    private static String PRESSURE_ANOMALY = 'Pressure Anomaly';
    private static String HUMIDITY_ANOMALY = 'Humidity Anomaly';

    public PageWrapper data{get; set;}
    
    public AnomalyReportPageController(){
        Map<String, AnomalyReportRow> reportTableData = new Map<String, AnomalyReportRow>();
        Integer totalAnomalies = 0;
        for (AggregateResult row : [SELECT COUNT(Id) counter, City_Name__c city, Type__c FROM Anomaly__c  WHERE Reported__c = false GROUP BY City_Name__c, Type__c]) {            
            String city = String.valueOf(row.get('city'));
            String type = String.valueOf(row.get('Type__c'));
            if (reportTableData.keySet().contains(city)) {
                AnomalyReportRow rec = reportTableData.get(city);
                if (type == TEMP_ANOMALY) {
                    rec.temperatureAnomaly = Integer.valueOf(row.get('counter'));
                }
                if (type == PRESSURE_ANOMALY) {
                    rec.pressureAnomaly = Integer.valueOf(row.get('counter'));
                }
                if (type == HUMIDITY_ANOMALY) {
                    rec.humidityAnomaly = Integer.valueOf(row.get('counter'));
                }
            } else {
                reportTableData.put(city, new AnomalyReportRow(row));
            }
            totalAnomalies += Integer.valueOf(row.get('counter'));
        }
        List<AggregateResult> minMaxDates = [SELECT MIN(CreatedDate) minDate, MAX(CreatedDate) maxDate
        FROM Anomaly__c WHERE Reported__c = false];
        if (reportTableData.values().size() == 0) {
            data = new PageWrapper();
        } else {
            data = new PageWrapper(reportTableData.values(), DateTime.valueOf(minMaxDates[0].get('minDate')), DateTime.valueOf(minMaxDates[0].get('maxDate')), totalAnomalies);
        }
    } 
    
    public class PageWrapper {
        public Datetime minDate{get; set;}
        public Datetime maxDate{get; set;}
        public Integer totalAnomalies{get; set;}
        public List<AnomalyReportRow> rows {get; set;}

        public PageWrapper(){
            this.totalAnomalies = null;
            this.rows = null;
            this.minDate = null;
            this.maxDate = null;
        }

        public PageWrapper(List<AnomalyReportRow> rows, Datetime minDate, Datetime maxDate, Integer totalAnomalies){
            this.totalAnomalies = totalAnomalies;
            this.rows = rows;
            this.minDate = minDate;
            this.maxDate = maxDate;
        }
    }

    public class AnomalyReportRow {
        
        public String anomalyCity {get; set;}
        public Integer temperatureAnomaly {get; set;}
        public Integer pressureAnomaly {get; set;}
        public Integer humidityAnomaly {get; set;}

        public AnomalyReportRow(AggregateResult ar) {
            anomalyCity = String.valueOf(ar.get('city'));
            String type = String.valueOf(ar.get('Type__c'));
            if (type == TEMP_ANOMALY) {
                    temperatureAnomaly = Integer.valueOf(ar.get('counter'));
            }
            if (type == PRESSURE_ANOMALY) {
                    pressureAnomaly = Integer.valueOf(ar.get('counter'));
            }
            if (type == HUMIDITY_ANOMALY) {
                    humidityAnomaly = Integer.valueOf(ar.get('counter'));
            }
        }
    }
}