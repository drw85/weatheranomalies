public class AnomalyViewComponentController {

    @AuraEnabled
    public static void getCurrentWeather() {
        OpenWeatherCalloutService.getWeatherFromOpenWeather();
    }

    @AuraEnabled
    public static List<Anomaly__c> getAnomalies(String searchingType, Boolean isOnlyUnreported) {
        if (searchingType == 'All') {
        if (isOnlyUnreported) {
            return removeReported(getAnomaliesAll());
        }
        return getAnomaliesAll();
       } else {
        if (isOnlyUnreported) {
            return removeReported(getTypeAnomalies(searchingType));
        }
        return getTypeAnomalies(searchingType);
       } 
    }

    private static List<Anomaly__c> getAnomaliesAll(){
        return [
            SELECT Id, Name, Type__c, Reported__c, CreatedDate, City__c, City__r.Name, Weather__c, Weather__r.Name
            FROM Anomaly__c
            ORDER BY CreatedDate DESC, Name DESC
        ];    
    }

    private static List<Anomaly__c> getTypeAnomalies(String type){
        return [
            SELECT Id, Name, Type__c, Reported__c, CreatedDate, City__c, City__r.Name, Weather__c, Weather__r.Name
            FROM Anomaly__c
            WHERE Type__c = :type
            ORDER BY CreatedDate DESC, Name DESC
        ];
    }

    private static List<Anomaly__c> removeReported(List<Anomaly__c> anomalies){
        List<Anomaly__c> result = new List<Anomaly__c>();
        for (Anomaly__c anomaly: anomalies) {
            if (!anomaly.Reported__c) {
                result.add(anomaly);
            }
        }
        return result;
    }

    @AuraEnabled
    public static void generateAnomaliesReport() {
        List<Anomaly__c> unreportedAnomalies = [SELECT Id, Reported__c FROM Anomaly__c WHERE Reported__c = false];
        for(Anomaly__c anomaly : unreportedAnomalies){
            anomaly.Reported__c = true;
        }
        Anomaly_Report__c report = new Anomaly_Report__c(Name = 'Anomaly Report ' + System.now().format('yyyy.MM.dd h:mm a'));
        insert report;
        ContentVersion cv = new ContentVersion();
        cv.Title = report.Name;
        cv.PathOnClient = cv.Title + '.pdf';
        cv.IsMajorVersion = true;
        cv.FirstPublishLocationId = report.Id;
        cv.VersionData = Page.AnomalyReportPage.getcontentAsPdf();
        insert cv;
        update unreportedAnomalies;
    }
}