@isTest
private class OpenWeatherCalloutServiceTest {

    @isTest static void testGetWeatherFromOpenWeather(){
        insert UtilTestWeatherCreator.createCitiesList(new List<String>{'Tokyo'});
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OpenWeatherCalloutMock()); 
        OpenWeatherCalloutService.getWeatherFromOpenWeather();
        Test.stopTest();
        List<Weather__c> weathers = [SELECT Id FROM Weather__c];
        System.assertEquals(1, weathers.size());
    }
    @isTest static void testSendRequest() {
        Test.setMock(HttpCalloutMock.class, new OpenWeatherCalloutMock()); 
        HttpResponse response = OpenWeatherCalloutService.sendRequest('Tokyo');
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        String expectedValue = '{"main":{"temp":284.17,"feels_like":280.35,"temp_min":283.71,"temp_max":284.82,"pressure":1022,"humidity":54},"visibility":10000,"wind":{"speed":3.09,"deg":20},"clouds":{"all":0},"name":"Tokyo"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
    }

    @isTest static void testGetRawData(){
        List<City__c> cities = UtilTestWeatherCreator.createCitiesList(new List<String>{'Tokyo'});
        insert cities;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OpenWeatherCalloutMock()); 
        List<OpenWeatherCalloutService.WeatherResponse> data = OpenWeatherCalloutService.getRawData(cities);
        for (OpenWeatherCalloutService.WeatherResponse wr : data) {
            System.assertEquals(wr.name, 'Tokyo');
            System.assertEquals(284.17, wr.main.temp);
        }
        Test.stopTest();
    }

    @isTest static void testGenerateWeather(){
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        OpenWeatherCalloutService.WeatherResponse wr = UtilTestWeatherCreator.createWeatherResponse(city);
        Weather__c weather = OpenWeatherCalloutService.generateWeather(wr);
        System.debug(weather);
    }
}