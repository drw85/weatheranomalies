public with sharing class AnomalyDetector {

    public Anomaly_Range_Configuration__c anomalyRangeSetting;

    public AnomalyDetector() {
        this.anomalyRangeSetting = Anomaly_Range_Configuration__c.getOrgDefaults();
    }

public List<Anomaly__c> detectAnomalies(List<Weather__c> weathers){
    List<Anomaly__c> detectedAnomalies = new List<Anomaly__c>();
    List<Id> weatherIds = new List<Id>();
    for (Weather__c weather : weathers) {
        weatherIds.add(weather.Id);
    }

List<Weather__c> weathersFromDb = [SELECT Id, City__c, City__r.Name, Humidity__c, Temperature__c, Pressure__c FROM Weather__c WHERE id IN :weatherIds];

for (Weather__c weather : weathersFromDb) {
    if (weather.Humidity__c > anomalyRangeSetting.Max_Humidity__c || weather.Humidity__c < anomalyRangeSetting.Min_Humidity__c) {
        detectedAnomalies.add(createAnomaly('Humidity Anomaly', weather));
    }
    if (weather.Temperature__c > anomalyRangeSetting.Max_Temperature__c || weather.Temperature__c < anomalyRangeSetting.Min_Temperature__c) {
        detectedAnomalies.add(createAnomaly('Temperature Anomaly', weather));
    }
    if (weather.Pressure__c > anomalyRangeSetting.Max_Pressure__c || weather.Pressure__c < anomalyRangeSetting.Min_Pressure__c) {
        detectedAnomalies.add(createAnomaly('Pressure Anomaly', weather));
    }
}
    return detectedAnomalies;
}
    
    private Anomaly__c createAnomaly(String anomalyType, Weather__c weather){
        return new Anomaly__c( City__c=weather.City__c,
            City_Name__c = weather.City__r.Name, 
            Reported__c = false, 
            Type__c = anomalyType,
            Weather__c = weather.Id);
    }
}