@isTest
global class OpenWeatherCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        
        String responseText = '{"main":{"temp":284.17,"feels_like":280.35,"temp_min":283.71,"temp_max":284.82,"pressure":1022,"humidity":54},"visibility":10000,"wind":{"speed":3.09,"deg":20},"clouds":{"all":0},"name":"Tokyo"}';

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(responseText);
        response.setStatusCode(200);
        return response; 
    }
}