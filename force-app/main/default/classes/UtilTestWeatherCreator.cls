public class UtilTestWeatherCreator {
    
private static final String WEATHER_JSON = '{"main":{"temp":284.17,"feels_like":280.35,"temp_min":283.71,"temp_max":284.82,"pressure":1022,"humidity":54},"visibility":10000,"wind":{"speed":3.09,"deg":20},"clouds":{"all":0},"name":"Tokyo"}';


    public static City__c createCity(String name){
        City__c city = new City__c(Name=name, 
        City_Code__c = name, 
        Country__c = 'JP');
        return city;
    }

public static List<City__c> createCitiesList(List<String> cityNames){
    List<City__c> cities = new List<City__c>();
    for(String name : cityNames){
        cities.add(createCity(name));
    }
    return cities;
}

public static OpenWeatherCalloutService.WeatherResponse createWeatherResponse(City__c city){
    OpenWeatherCalloutService.WeatherResponse wr = (OpenWeatherCalloutService.WeatherResponse)JSON.deserialize(WEATHER_JSON, OpenWeatherCalloutService.WeatherResponse.class);
    wr.city = city;
    wr.name = city.Name;
    return wr;
}

    public static Weather__c createSingleWeatherWithNoAnomalies(Anomaly_Range_Configuration__c anomalyRangeSetting, City__c city) {
        return new Weather__c(City__c = city.Id,
                        Temperature__c = anomalyRangeSetting.Max_Temperature__c,  
                        Cloudiness__c = 2,
                        Humidity__c  = anomalyRangeSetting.Max_Humidity__c,
                        Maximum_Temperature__c = anomalyRangeSetting.Max_Temperature__c,
                        Minimal_Temperature__c = anomalyRangeSetting.Min_Temperature__c,
                        Pressure__c = anomalyRangeSetting.Max_Pressure__c,
                        Temperature_Feels_Like__c = anomalyRangeSetting.Max_Temperature__c,
                        Visibility__c = 13000,
                        Wind_Speed__c = 1
        );
    }

    public static Weather__c createSingleWeatherWithTemperatureAnomaly(Anomaly_Range_Configuration__c anomalyRangeSetting, City__c city) {
        Weather__c weather = createSingleWeatherWithNoAnomalies(anomalyRangeSetting, city);
        weather.Temperature__c += 1;
        return weather;
    }

    public static Weather__c createSingleWeatherWithPressureAnomaly(Anomaly_Range_Configuration__c anomalyRangeSetting, City__c city) {
        Weather__c weather = createSingleWeatherWithNoAnomalies(anomalyRangeSetting, city);
        weather.Pressure__c += 1;
        return weather;
    }

    public static Weather__c createSingleWeatherWithHumidityAnomaly(Anomaly_Range_Configuration__c anomalyRangeSetting, City__c city) {
        Weather__c weather = createSingleWeatherWithNoAnomalies(anomalyRangeSetting, city);
        weather.Humidity__c += 1;
        return weather;
    }

    public static List<Weather__c> generateWeathersList(Weather__c weather, Integer size) {
        List<Weather__c> weathers = new List<Weather__c>();
        for (Integer i = 0; i < size; i++) {
            weathers.add(weather.clone());
        }
        return weathers;
    }
}