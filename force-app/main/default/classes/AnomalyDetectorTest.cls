@IsTest
public class AnomalyDetectorTest {
    
    @testSetup static void setup() {
        Anomaly_Range_Configuration__c anomalyRangeSetting = new Anomaly_Range_Configuration__c(
            Name = 'Test Setting',
            Max_Humidity__c = 105,
            Max_Pressure__c = 1006,
            Max_Temperature__c = 290,
            Min_Humidity__c = 101, 
            Min_Pressure__c = 1001,
            Min_Temperature__c = 281
        );
        insert anomalyRangeSetting;
    }

    @isTest
    static void shouldNotDetectAnyAnomalyWhenWeatherHasNoAnomaliesAndThereIsOnlyOneWeather() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithNoAnomalies(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = new List<Weather__c>{weather};
        System.assertEquals(0, new AnomalyDetector().detectAnomalies(weathers).size());
    }

    @isTest
    static void shouldNotDetectAnyAnomalyWhenWeatherHasNoAnomaliesAndThereIsListOfWeathers() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithNoAnomalies(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, 20);   
        System.assertEquals(0, new AnomalyDetector().detectAnomalies(weathers).size());
    }

    @isTest
    static void shouldDetectTemperatureAnomalyWhenWeatherHasTemperatureAnomalyAndThereIsOnlyOneWeather() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithTemperatureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = new List<Weather__c>();
        weathers.add(weather);   
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Temperature Anomaly', anomaly.Type__c);
        }
    }

    @isTest
    static void shouldDetectTemperatureAnomaliesWhenWeathersHasTemperatureAnomaliesAndThereIsListOfWeathers() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithTemperatureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, 20);  
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(20, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Temperature Anomaly', anomaly.Type__c);
        }
    }

    @isTest
    static void shouldDetectPressureAnomalyWhenWeatherHasPressureAnomalyAndThereIsOnlyOneWeather() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithPressureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = new List<Weather__c>();
        weathers.add(weather);   
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Pressure Anomaly', anomaly.Type__c);
        }
    }

    @isTest
    static void shouldDetectPressureAnomaliesWhenWeathersHasPressureAnomaliesAndThereIsListOfWeathers() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithPressureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, 20);  
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(20, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Pressure Anomaly', anomaly.Type__c);
        }
    }

    @isTest
    static void shouldDetectHumidityAnomalyWhenWeatherHasHumidityAnomalyAndThereIsOnlyOneWeather() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithHumidityAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = new List<Weather__c>();
        weathers.add(weather);   
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Humidity Anomaly', anomaly.Type__c);
        }
    }

    @isTest
    static void shouldDetectHumidityAnomaliesWhenWeathersHasHumidityAnomaliesAndThereIsListOfWeathers() {
        City__c city = UtilTestWeatherCreator.createCity('Tokyo');
        insert city;
        Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithHumidityAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
        List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, 20);  
        insert weathers;
        List<Anomaly__c> detectedAnomalies = new AnomalyDetector().detectAnomalies(weathers);
        System.assertEquals(20, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
           System.assertEquals('Humidity Anomaly', anomaly.Type__c);
        }
    }
}