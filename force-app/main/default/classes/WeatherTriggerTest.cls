@isTest
public class WeatherTriggerTest {

private static Integer DEFAULT_LIST_SIZE = 20;

@testSetup static void setup() {
  Anomaly_Range_Configuration__c anomalyRangeSetting = new Anomaly_Range_Configuration__c(
      Name = 'Test Setting',
      Max_Humidity__c = 105,
      Max_Pressure__c = 1006,
      Max_Temperature__c = 290,
      Min_Humidity__c = 101, 
      Min_Pressure__c = 1001,
      Min_Temperature__c = 281
  );
  insert anomalyRangeSetting;
}

    @isTest 
    static void shouldDetectTemperatureAnomalyWhenWeatherHasTemperatureAnomalyAndThereIsOnlyOneWeather() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithTemperatureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = new List<Weather__c>();
      weathers.add(weather);         
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id = :sr.getId()];
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Temperature Anomaly', anomaly.Type__c);
        }
      }
    }        

    @isTest 
    static void shouldDetectTemperatureAnomaliesWhenWeathersHasTemperatureAnomaliesAndThereIsListOfWeathers() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithTemperatureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, DEFAULT_LIST_SIZE);
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      Set<Id> insertedWeathersIds = new Set<Id>();
      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        insertedWeathersIds.add(sr.getId());
      }

      List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id IN :insertedWeathersIds];
      System.assertEquals(DEFAULT_LIST_SIZE, detectedAnomalies.size());
    
      for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Temperature Anomaly', anomaly.Type__c);
      }
    }
    
    @isTest 
    static void shouldDetectPressureAnomalyWhenWeatherHasPressureAnomalyAndThereIsOnlyOneWeather() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithPressureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = new List<Weather__c>();
      weathers.add(weather);         
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id = :sr.getId()];
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Pressure Anomaly', anomaly.Type__c);
        }
      }
    }        

    @isTest 
    static void shouldDetectPressureAnomaliesWhenWeathersHasPressureAnomaliesAndThereIsListOfWeathers() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithPressureAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, DEFAULT_LIST_SIZE);
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      Set<Id> insertedWeathersIds = new Set<Id>();
      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        insertedWeathersIds.add(sr.getId());
      }

      List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id IN :insertedWeathersIds];
      System.assertEquals(DEFAULT_LIST_SIZE, detectedAnomalies.size());
    
      for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Pressure Anomaly', anomaly.Type__c);
      }
    }

    @isTest 
    static void shouldDetectHumidityAnomalyWhenWeatherHasHumidityAnomalyAndThereIsOnlyOneWeather() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithHumidityAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = new List<Weather__c>();
      weathers.add(weather);         
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id = :sr.getId()];
        System.assertEquals(1, detectedAnomalies.size());
        for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Humidity Anomaly', anomaly.Type__c);
        }
      }
    }        

    @isTest 
    static void shouldDetectHumidityAnomaliesWhenWeathersHasHumidityAnomaliesAndThereIsListOfWeathers() {
      City__c city = UtilTestWeatherCreator.createCity('Tokyo');
      insert city;
      Weather__c weather = UtilTestWeatherCreator.createSingleWeatherWithHumidityAnomaly(Anomaly_Range_Configuration__c.getOrgDefaults(), city);
      List<Weather__c> weathers = UtilTestWeatherCreator.generateWeathersList(weather, DEFAULT_LIST_SIZE);
      
      Test.startTest();
      List<Database.SaveResult> saveResults = Database.insert(weathers, false);
      Test.stopTest();

      Set<Id> insertedWeathersIds = new Set<Id>();
      for (Database.SaveResult sr : saveResults) {
        System.assert(sr.isSuccess());
        insertedWeathersIds.add(sr.getId());
      }

      List<Anomaly__c> detectedAnomalies = [SELECT id, Type__c, Weather__r.Id from Anomaly__c WHERE Weather__r.Id IN :insertedWeathersIds];
      System.assertEquals(DEFAULT_LIST_SIZE, detectedAnomalies.size());
    
      for (Anomaly__c anomaly : detectedAnomalies) {
          System.assertEquals('Humidity Anomaly', anomaly.Type__c);
      }
    }
}